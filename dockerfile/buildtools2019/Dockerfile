# escape=`
# コマンドのエスケープにバックスラッシュを使うのはWindowsの体系ではアレなので、バッククオートを使用するよう定義。

# servercore系ではVSIXのインストールができない仕様なため、windowsベースイメージを使用。
# ビルド1809を使っているのは、Windows ContainerはホストOSのビルドより古いものしか使えないため。
# 最新は19xxだが、少し古いもので固定。SACなのでできれば定期的に更新したい。
FROM mcr.microsoft.com/windows:1809

# Restore the default Windows shell for correct batch processing.
SHELL ["cmd", "/S", "/C"]

# 参照: https://stackoverrun.com/ja/q/10100917
# VisualStudio 2017のインストール。
# TODO プロダクトキーを入力する必要がライセンス上あれば、インストール時にオプション追加する。
# （RUNを3回に分けてるのはデバッグ用、上からワークロード単位、パッケージ単位（参考ページを踏襲）、パッケージ単位（オリジナル）
ADD https://aka.ms/vs/16/release/vs_community.exe C:\TEMP\vs_community.exe

RUN  C:\TEMP\vs_community.exe --quiet --wait --norestart --nocache `
    --add Microsoft.VisualStudio.Workload.CoreEditor `
    --add Microsoft.VisualStudio.Workload.ManagedDesktop `
    --add Microsoft.VisualStudio.Workload.NativeDesktop `
    --add Microsoft.VisualStudio.Component.Static.Analysis.Tools `
    || IF "%ERRORLEVEL%"=="3010" EXIT 0


# ビルドスクリプトの実行のため、Pythonを追加。バージョンはとりあえず現時点での最新。 pytestも入れとく。
ADD https://www.python.org/ftp/python/3.8.2/python-3.8.2.exe C:\TEMP\python-3.8.2.exe
RUN C:\TEMP\python-3.8.2.exe /quiet InstallAllUsers=1 PrependPath=1 Include_test=0
RUN pip install pytest

# VisualStudio 2017にInstallerProjectsを導入。
# VSIXInstallerは同期待ちの機能が無いため、PowerShell経由でインストーラ実行することで同期待ち。
ADD https://visualstudioclient.gallerycdn.vsassets.io/extensions/visualstudioclient/microsoftvisualstudio2017installerprojects/0.9.8/1588102164123/InstallerProjects.vsix C:\TEMP\InstallerProjects.vsix
ADD .\resources\install_installerprojects.ps1 C:\TEMP\install_installerprojects.ps1
RUN powershell -NoProfile -ExecutionPolicy Unrestricted C:\TEMP\install_installerprojects.ps1

# Javaをインストールし、PMD-CPDをC:\に展開しておく。
ADD https://javadl.oracle.com/webapps/download/AutoDL?BundleId=242990_a4634525489241b9a9e1aa73d9e118e6 C:\TEMP\jre-8u261-windows-x64.exe
RUN C:\TEMP\jre-8u261-windows-x64.exe /s /L C:\JavaInstall.log
ADD https://github.com/pmd/pmd/releases/download/pmd_releases%2F6.25.0/pmd-bin-6.25.0.zip C:\TEMP\pmd-bin-6.25.0.zip
RUN powershell /c Expand-Archive -Path C:\TEMP\pmd-bin-6.25.0.zip -DestinationPath C:\pmd
RUN setx PATH "%PATH%;C:\pmd\pmd-bin-6.25.0\bin"

# clocをインストール（C:\toolsに放り込むだけ）
ADD https://jaist.dl.sourceforge.net/project/cloc/cloc/v1.64/cloc-1.64.exe C:\tools\cloc.exe
RUN setx PATH "%PATH%;C:\tools"

# SVNを入れとく
ADD https://www.visualsvn.com/files/Apache-Subversion-1.14.0.zip C:\TEMP\Apache-Subversion-1.14.0.zip
RUN powershell /c Expand-Archive -Path C:\TEMP\Apache-Subversion-1.14.0.zip -DestinationPath C:\svn
RUN setx PATH "%PATH%;C:\svn\bin"

# TODO Intel Compilerを適切にインストールする。

# ADD .\resources\parallel_studio_xe_2017_update8_cluster_edition_online_setup.exe C:\TEMP\parallel_studio_xe_2017_update8_cluster_edition_online_setup.exe
# ADD .\resources\parallel_studio_xe_2017_update8_setup.exe C:\TEMP\parallel_studio_xe_2017_update8_setup.exe
# ADD .\resources\install_intelcompiler.ps1 C:\TEMP\install_intelcompiler.ps1
# RUN C:\TEMP\parallel_studio_xe_2017_update8_cluster_edition_online_setup.exe --silent -a install --eula=accept --output=C:\TEMP\ICInstall.log --eval

# エントリポイントとしてVisualStudio開発者コマンドプロンプトを登録しておく。ここは必要に応じて変更しても良いと思う。
ENTRYPOINT ["C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\Common7\\Tools\\VsDevCmd.bat", "&&", "powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]
