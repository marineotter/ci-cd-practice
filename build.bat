call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\VsDevCmd.bat"

cd %~dp0projects
MSBuild TestTargetApp.sln /t:clean;rebuild /p:Configuration=Release;Platform="Any CPU" >build.log
if %ERRORLEVEL% neq 0 (
    echo ErrorLevel:%ERRORLEVEL%
    exit /b 1
)
exit /b 0
