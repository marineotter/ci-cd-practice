import unittest
import os

TARGET_INSTALLED_FILE = r"C:\Program Files (x86)\kawauso-lab\TestTargetApp\TestTargetApp.exe"
TARGET_INSTALLED_VERSIONFILE = r"C:\Program Files (x86)\kawauso-lab\TestTargetApp\BuildInfo.txt"


class InstallCheck(unittest.TestCase):
    def test_installed_file(self):
        self.assertTrue(os.path.exists(TARGET_INSTALLED_FILE))

    def test_installed_versionfile(self):
        with open(TARGET_INSTALLED_VERSIONFILE) as rf:
            data = rf.read()
        print(data)
        self.assertTrue("BuildId=" in data)
        self.assertTrue("SrcVersion=" in data)


if __name__ == '__main__':
    unittest.main()
