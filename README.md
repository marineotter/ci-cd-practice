## 何か

CI/CD思想の実験用プロジェクト。ほとんど備忘録。

今のところ、Jenkinsを使ったCIを勉強しながら書いています。

## 試し方（環境）

* 必要な環境
  - Windows PC (RAM 8GB以上 空きディスク40GB以上) 1機
* 試す手順
  1. 環境にDocker Desktopを入れ、Windows Containerモードにしておきます。  
  また、storage-optsを40GB以上にしておきます。
  2. Dockerfile/buildtools2019のbuild.batを実行し、Dockerイメージ「buildtools2019」を用意します。
  3. 環境にJenkinsをインストールし、色々とプラグインを入れます。下記が要るはずです。
    - Pipeline: Multibranch
    - Blue Ocean
    - SLOCCount Plug-in
    - Warnings Next Generation Plugin
  4. リポジトリをmultibranch jobに食わせます。  
  **GitLabから直接食わせるのは実行環境に対してセキュリティ上の深刻な問題を生じさせるので、できればローカルでSVNサーバなどを建てて下さい。**  
  （私が意図して/意図せずして害のあるコードをコミットしてしまった場合、その影響を受けます。）

## 解説

ルートディレクトリに色々散らかしてしまっています。

以下のVisual Studio 2019のソリューション、及びPythonで書いた単純なユニットテストをCIの対象として用意しています。

* projectsディレクトリ下
  * TestTargetApp.sln
    - C#のGUIアプリケーションです。すごく簡易。
  * Installer.sln
    - 上記アプリケーションをインストールするインストーラを作ります。こっちも簡易。
* build.bat
  - GUIアプリケーション単体をビルドするスクリプトです。
* makeinstaller.bat
  - インストーラを作成するスクリプトです。
* testsディレクトリとpytest.ini
  - pythonで書いたテストを溜めておくディレクトリです。実行はpytestをつかいます。

以下で用意しているものが、今回遊んでみたかった主なものです。

* Jenkinsfile
  - **このプロジェクトで試したかったメイン。上述のビルド、テストを継続的インテグレーションしてみるための定義ファイル。**
* dockerfileディレクトリ
  - **上記Jenkinsfileで使ってるコンテナ環境（buildtools2019）を生成するためのリソースを入れています。**  
  dockerのwindows containerを使用し、かつコンテナ内にvisual studioをインストールする邪道ですが、十分使えています。  
  これも実験的です。

## 何をやってるか

JenkinsfileをJenkinsのmulti brunch pipeline ジョブに食わせて、自動でビルド～インストーラ作成～インストーラテストまでをやっています。

## 何をやりたいか

レガシーなWindowsアプリケーションで、どこまでCI/CD思想で開発者が楽をでき、安全に開発できるか極めてみたいです。

## イメージ

こんな感じでワークフローが可視化出来ます。

この程度だと見掛け倒しな面もありますが、実務で使うと十分に感動できるぐらいにはワークフローが透明化出来ます。

<img src="docs/jenkins-blueocean-image.png" width=45% > <img src="docs/jenkins-metrics-image.png" width=45% >

