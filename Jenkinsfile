// パイプラインで使う大域変数を確保しておく
def SRC_VERSION

// パイプライン本体の宣言
pipeline {
  agent none
  options {
    skipDefaultCheckout() // Changelogが二重に記録されてしまう現象のワークアラウンド。最初の一発でstashして使い回すから、毎回checkoutする必要がない。
  }
  stages {
    stage('Checkout') {
      // ソースコードをチェックアウトする。（gitlabから指定した場合はgitlabからcloneします。）
      // チェックアウトしたソースコードは名前「src」でstash（保存）しておきます。
      // ここで保存したソースコードを次から使いまわしてビルドなどを実行します。
      agent {
        docker {
          image 'buildtools2019'
          label 'docker-windows'
        }
      }
      steps {
        ws("${env.WORKSPACE}/checkout") {
          powershell 'Convert-Path .' // デバッグ用にワークスペースの場所を出力する。
          deleteDir() // ワークスペースをきれいにしておく。
          checkout(changelog: true, scm: scm) // チェックアウトする。
          // リビジョンを取得し、SRC_REVISION変数に入れておく。 SVN以外の場合失敗するかもなので、catchErrorしておく。
          catchError(buildResult: 'SUCCESS', stageResult: 'SUCCESS') {
            script {
              powershell 'svn upgrade'
              SRC_VERSION = powershell(script: 'svn info --show-item revision', returnStdout: true)
            }
          }
          echo "SRC_VERSION=${SRC_VERSION}"
          archiveArtifacts artifacts: 'Jenkinsfile', fingerprint: true // このJenkinsfileをいつでも見れるように保存しておく。
          stash name: 'src' // 名前「src」で保存する。
        }
      }
    }
    stage('Build and Analyze') {
      parallel {
        stage('Build: All') {
          agent {
            docker {
              image 'buildtools2019'
              label 'docker-windows'
            }
          }
          steps {
            ws("${env.WORKSPACE}/build") {
              powershell 'Convert-Path .' // デバッグ用にワークスペースの場所を出力する。
              deleteDir() // ワークスペースをきれいにしておく。
              unstash 'src' // 名前「src」で保存したやつを引っ張り出す。
              echo "SRC_VERSION=${SRC_VERSION}"
              // ビルドとインストーラ作成する。
              powershell 'cmd /c build.bat'
              writeFile(file: "projects/BuildInfo.txt", text: "BuildId=${env.BUILD_NUMBER}\r\nSrcVersion=${SRC_VERSION}")
              powershell 'type projects\\BuildInfo.txt'
              powershell 'cmd /c makeinstaller.bat'
              // 成果物の公開と引き継ぎ
              stash name: 'installer', includes: 'projects/Release/*' // 作ったインストーラを保存しておく。
              archiveArtifacts artifacts: 'projects/Release/*', fingerprint: true // 作ったインストーラをJenkinsでアーカイブする。
            }
          }
          post {
            always {
              ws("${env.WORKSPACE}/build") {
                recordIssues tool: msBuild(id:'build_obj', name: 'Build Object', pattern: 'projects/build.log')
                recordIssues tool: msBuild(id:'build_pkg', name: 'Build Package', pattern: 'projects/build_installer.log')
                archiveArtifacts artifacts: 'projects/build*.log'
              }
            }
          }
        }
        stage('Analyze: Code-clone') {
          // ソースコード解析タスク。コードクローンを解析します。（実験的）
          agent {
            docker {
              image 'buildtools2019'
              label 'docker-windows'
            }
          }
          steps {
            ws("${env.WORKSPACE}/analyze_cpd") {
              powershell 'Convert-Path .' // デバッグ用にワークスペースの場所を出力する。
              deleteDir()
              unstash 'src'
              echo "SRC_VERSION=${SRC_VERSION}"
              catchError(buildResult: 'SUCCESS', stageResult: 'UNSTABLE') {
                powershell 'cpd --minimum-tokens 30 --language cs --format xml --files .\\projects\\ | Out-File -Encoding UTF8 analyze_cpd_result.xml'
              }
              recordIssues tool: cpd(pattern: 'analyze_cpd_result.xml')
              archiveArtifacts artifacts: 'analyze_cpd_result.xml', fingerprint: false
            }
          }
        }
        stage('Analyze: LoC') {
          // ソースコード解析タスク。コードのLoCを解析します。（実験的）
          agent {
            docker {
              image 'buildtools2019'
              label 'docker-windows'
            }
          }
          steps {
            ws("${env.WORKSPACE}/analyze_cloc") {
              powershell 'Convert-Path .' // デバッグ用にワークスペースの場所を出力する。
              deleteDir()
              unstash 'src'
              echo "SRC_VERSION=${SRC_VERSION}"
              catchError(buildResult: 'SUCCESS', stageResult: 'UNSTABLE') {
                powershell 'cloc --by-file --xml --out=analyze_cloc_result.xml .\\'
              }
              sloccountPublish(pattern: 'analyze_cloc_result.xml', numBuildsInGraph: -1)
              archiveArtifacts artifacts: 'analyze_cloc_result.xml', fingerprint: false
            }
          }
        }
      }
    }
    stage('Test') {
      parallel {
        stage('Test: installer') {
          agent {
            docker {
              image 'buildtools2019'
              label 'docker-windows'
            }
          }
          steps {
            ws("${env.WORKSPACE}/install_test") {
              powershell 'Convert-Path .' // デバッグ用にワークスペースの場所を出力する。
              deleteDir()
              unstash 'installer'
              unstash 'src'
              echo "SRC_VERSION=${SRC_VERSION}"
              powershell 'msiexec /qn /norestart /i projects\\Release\\Installer.msi' // 作ったインストーラを使って、「隔離されたDockerコンテナ内に」インストールしてみる。
              catchError(buildResult: 'UNSTABLE', stageResult: 'UNSTABLE') {
                  powershell 'pytest --junitxml=test_installer_result.xml' // テストを実行する。
              }
              junit 'test_installer_result.xml' // テスト結果をJenkinsで集計する。
              archiveArtifacts artifacts: 'test_installer_result.xml', fingerprint: false
            }
          }
        }
      }
    }
  }
}
